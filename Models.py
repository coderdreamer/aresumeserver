import sqlalchemy
from sqlalchemy import *
from sqlalchemy.orm import *
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from datetime import datetime

Engine = None
Session = None

#Defination meta data
Base = declarative_base();

class Cloud(object):
    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    __table_args__ = {'mysql_charset': 'utf8'}

    cid =  Column(Integer, primary_key=True)

class Candidate(Cloud, Base):
    #Attribute
    name = Column(String(64), nullable=False, default='John Doe')
    gender = Column(Integer, nullable=False, default=1)
    dateOfBirth = Column(DATE)
    photo = Column(String(1024))
    #Relationships
    contact = relationship("Contact", backref=Cloud.__tablename__,
        primaryjoin=lambda: Candidate.cid==Contact.id_candidate)
    education = relationship("Education", backref=Cloud.__tablename__,
        primaryjoin=lambda: Candidate.cid==Education.id_candidate)
    experience = relationship("Experience", backref=Cloud.__tablename__,
        primaryjoin=lambda: Candidate.cid==Experience.id_candidate)
    skill = relationship("Skill", backref=Cloud.__tablename__,
        primaryjoin=lambda: Candidate.cid==Skill.id_candidate)
    honour = relationship("Honour", backref=Cloud.__tablename__,
        primaryjoin=lambda: Candidate.cid==Honour.id_candidate)

    @staticmethod
    def candidateWithID(id):
        s = session()
        results = s.query(Candidate).filter_by(cid=id)
        if results.count() == 1:
            return results[0]

class Contact(Cloud, Base):
    #Attribute
    type = Column(Integer, nullable=False, default=0)
    value = Column(String(64))
    #Relationships
    id_candidate = Column(Integer, ForeignKey('candidate.cid'))

class Education(Cloud, Base):
    #Attribute
    degree = Column(Integer)
    major = Column(String(64))
    university = Column(String(64))
    #Relationships
    id_candidate = Column(Integer, ForeignKey('candidate.cid'))

class Experience(Cloud, Base):
    #Attribute
    title = Column(String(64), nullable=False, default='Work for Apple')
    exDescription = Column(String(2048))
    link = Column(String(1024))
    #Relationships
    image = relationship('Image', primaryjoin=lambda: Experience.cid==Image.id_experience)
    id_candidate = Column(Integer, ForeignKey('candidate.cid'))

class Skill(Cloud, Base):
    #Attribute
    title = Column(String(64), nullable=False, default='Objective-C')
    skillDescription = Column(String(2048))
    #Relationships
    image = relationship('Image', primaryjoin=lambda: Skill.cid==Image.id_skill)
    id_candidate = Column(Integer, ForeignKey('candidate.cid'))

class Honour(Cloud, Base):
    #Attribute
    title = Column(String(64), nullable=False, default='Nobel Prize')
    date = Column(DATE)
    #Relationships
    id_candidate = Column(Integer, ForeignKey('candidate.cid'))

class Image(Cloud, Base):
    #Attribute
    path = Column(String(1024))
    index = Column(Integer)
    #Relationships
    id_experience = Column(Integer, ForeignKey('experience.cid'))
    id_skill = Column(Integer, ForeignKey('skill.cid'))

def engine():
    global Engine
    if Engine == None:
        dbUser = 'root'
        dbPassword = 'root'
        dbHost = '127.0.0.1'
        dbName = 'mysql'
        try:
            import json
            import sys
            filePath = 'MySQLInfo';
            projectPath = sys.path[0]
            if projectPath != '':
                filePath = projectPath + '/' + 'MySQLInfo';
            with open(filePath, encoding='utf-8') as file:
                mysqlDic = json.load(file)
                dbUser = mysqlDic['dbUser']
                dbPassword = mysqlDic['dbPassword']
                dbHost = mysqlDic['dbHost']
                dbName = mysqlDic['dbName']
        finally:
            Engine = create_engine('mysql+mysqlconnector://' + dbUser + ':' + dbPassword + '@' + dbHost + '/' + dbName)
    return Engine

def session():
    global Session
    if Session == None:
        e = engine()
        Session = sessionmaker(bind=e)
    session = Session()
    return session

def candidateWithID(id):
    return candidateDefault()
    return Candidate.candidateWithID(id)

def candidateDefault():
    s = session()
    results = s.query(Candidate)
    if results.count() > 0:
        return results.all()[0]