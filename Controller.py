import tornado.httpserver
import tornado.ioloop
import tornado.web

from Serializer import dumps
from Models import candidateWithID

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write('Welcome to A Resume.')

class Candidate(tornado.web.RequestHandler):
    def get(self):
        id = self.get_argument("id")
        c = candidateWithID(id)
        response = dumps(c)
        self.write(response)

class RelExperience(tornado.web.RequestHandler):
    def get(self):
        id = self.get_argument("id")
        c = candidateWithID(id)
        response = dumps(c.experience)
        self.write(response)

class RelSkill(tornado.web.RequestHandler):
    def get(self):
        id = self.get_argument("id")
        c = candidateWithID(id)
        response = dumps(c.skill)
        self.write(response)

class RelHonour(tornado.web.RequestHandler):
    def get(self):
        id = self.get_argument("id")
        c = candidateWithID(id)
        response = dumps(c.honour)
        self.write(response)

def start(port=8071):
    application = tornado.web.Application([
        ("/", MainHandler),
        ("/" + Candidate.__name__, Candidate),
        ("/" + RelExperience.__name__, RelExperience),
        ("/" + RelSkill.__name__, RelSkill),
        ("/" + RelHonour.__name__, RelHonour),
        ])
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(port)
    tornado.ioloop.IOLoop.instance().start()
