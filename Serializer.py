from Models import  Candidate, Contact, Education, Experience, Skill, Honour, Image, Cloud
from datetime import date

def toJson(p_object):
    if isinstance(p_object, Candidate):
        candidate = p_object
        return classToJson(candidate, {'Nam':candidate.name,
                                       'Gen':candidate.gender,
                                       'DOB':candidate.dateOfBirth,
                                       'Pho':candidate.photo,
                                       'Con':candidate.contact,
                                       'Edu':candidate.education})
    if isinstance(p_object, Contact):
        contact = p_object
        return classToJson(contact, {'Type':contact.type,
                                     'Value':contact.value})
    if isinstance(p_object, Education):
        education = p_object
        return classToJson(education, {'Degree':education.degree,
                                       'Major':education.major,
                                       'Uni':education.university})
    if isinstance(p_object, Experience):
        experience = p_object
        return classToJson(experience, {'Title':experience.title,
                                       'Description':experience.exDescription,
                                       'Link':experience.link,
                                       'Image':experience.image})
    if isinstance(p_object, Skill):
        skill = p_object
        return classToJson(skill, {'Title':skill.title,
                                   'Description':skill.skillDescription,
                                   'Image':skill.image})
    if isinstance(p_object, Honour):
        honour = p_object
        return classToJson(honour, {'Title':honour.title,
                                   'Date':honour.date})
    if isinstance(p_object, Image):
        image = p_object
        return classToJson(image, {'Path':image.path,
                                   'Index':image.index})
    if isinstance(p_object, date):
        theDate = p_object;
        return theDate.strftime("%Y-%m-%d")
    raise TypeError(repr(p_object) + ' is not JSON serializable')

def classToJson(p_object, attributes):
    if isinstance(attributes, dict):
        j = {'__class__':p_object.__class__.__name__}
        if isinstance(p_object, Cloud):
            j['cid'] = p_object.cid
        for key, attr in attributes.items():
            if isEffective(key) and isEffective(attr):
                j[key] = attr
        return j
    else:
        return


def jsonCandidate(candidate):
    j = {'__class__':Candidate.__name__,
         'Nam':candidate.name}
    if isEffective(candidate.gender):
        j['Gen'] = candidate.gender
    if isEffective(candidate.dateOfBirth):
        j['DOB'] = candidate.dateOfBirth
    if isEffective(candidate.contact):
        j['Con'] = candidate.contact
    if isEffective(candidate.education):
        j['Edu'] = candidate.education
    return j

def isEffective(p_object):
    if p_object == None:
        return False
    if hasattr(p_object, '__len__') and p_object.__len__() == 0:
        return False
    if isinstance(p_object, str) and p_object.strip().__len__() == 0:
        return False
    return True

import json
def dumps(p_object):
    return json.dumps(p_object, default=toJson, ensure_ascii=False)